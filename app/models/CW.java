/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  models.pojo
 * @FileName: CacheWrapper.java
 * @author:   Andrew Yuan
 * @date:     2015/2/1, 上午 09:45:25
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package models;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * The Class CacheWrapper.
 * 
 * <pre>
 * 處理cache相關
 * </pre>
 */
public class CW<T> implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -7527568225789847749L;

  /** 設定此cache的初始時間. */
  private long settleTime;

  /** cache內容物. */
  private T cacheObject;

  public CW(T obj, long settleTime) {
    this.settleTime = settleTime;
    this.cacheObject = obj;
  }

  public long getSettleTime() {
    return settleTime;
  }

  public void setSettleTime(long settleTime) {
    this.settleTime = settleTime;
  }

  public T getCacheObject() {
    return cacheObject;
  }

  public void setCacheObject(T cacheObject) {
    this.cacheObject = cacheObject;
  }

  /** double check cache time,如果存在太老的cache回傳true. */
  public Boolean validateCacheTime(long now, long timeInterval) {
    if (now - this.settleTime > TimeUnit.SECONDS.toMillis(timeInterval))
      return true;
    else
      return false;
  }

}
