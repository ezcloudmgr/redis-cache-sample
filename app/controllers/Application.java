package controllers;

import java.util.HashMap;

import models.CW;
import play.Play;
import play.mvc.Controller;
import play.mvc.Result;
import plugins.EzRedisPlugin;
import views.html.index;

public class Application extends Controller {

  private static EzRedisPlugin rCache = Play.application().plugin(
      EzRedisPlugin.class);

  public static Result index() {

    HashMap<String, String> hash = new HashMap<>();
    hash.put("abc", "xyz");

    
    CW cc = new CW(hash, System.currentTimeMillis());
    
    rCache.set("cwKey", cc);
    rCache.get("cwKey");
    
    rCache.set("cacheKey", hash);

    
    
    
    rCache.setJSON("jsonKey", hash);

    System.out.println(rCache.getJSON("jsonKey", HashMap.class).get("abc"));

    rCache.expire("cacheKey", 10);

    @SuppressWarnings("unchecked")
    HashMap<String, String> cacheHash = (HashMap<String, String>) rCache
        .get("cacheKey");

    if (cacheHash != null)
      System.out.println(cacheHash.get("abc"));

    return ok(index.render("Your new application is ready."));
  }
}
