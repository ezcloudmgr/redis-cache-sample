name := "redis-cache-sample"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
	"eztravel.play" % "ez-play-rediscache-api" % "1.0-SNAPSHOT"
)

resolvers += (
    "Eztravel Maven Repository" at "http://rtc3.eztravel.com.tw"
)

play.Project.playJavaSettings
